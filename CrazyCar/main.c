/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "HAL/hal_general.h"
#include "HAL/hal_gpio.h"
#include "DL/driver_general.h"
#include "DL/driver_ir.h"
#include "DL/driver_lcd.h"
#include "DL/driver_aktorik.h"
#include "AL/algorithm_general.h"
#include "AL/algorithm_steering.h"
#include "AL/algorithm_throttle.h"
#include "DL/driver_rpm.h"
// --------------------------------------------------

extern PIDParams steering_params;
extern PIDParams throttle_params;
extern ButtonCom button_com;
extern SensorParams sensor_params;

void main(void)
{
	halInit();
	driverInit();

	while (1) {
		driverLCDWriteUint(steering_params.kd, 1, 0);
	}

}
