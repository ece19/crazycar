/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------
#include "hal_adc12.h"
#include "hal_dma.h"
// --------------------------------------------------

__interrupt void DMA(void);

extern ADC12Com adc12_com;

char dma_counter = 0;

void halDmaInit(){
	DMACTL0	|=	DMA0TSEL_24;								// Set the ADC12 as a input trigger source

	DMA0CTL	|=	DMADT_5;									// repeated block transfer

	DMA0CTL	|=	DMADSTINCR_3;								// destination address is incremented
	DMA0CTL	|=	DMASRCINCR_3;								// source address is incremented
	DMA0CTL	&=	~DMADSTBYTE;
	DMA0CTL	&=	~DMASRCBYTE;

	DMA0SA	=	&ADC12MEM0;									// sets the starting address of the memory transfer
	DMA0DA	=	adc12_com.adc_buffer[dma_counter];			// sets the destination address
	DMA0SZ 	= 	4;											// sets the size of the data - transfer

	DMA0CTL	|=	DMAIE; 					 					// interrupt enable for the DMA
	DMA0CTL	|=	DMAEN; 										// enables the DMA
}

#pragma vector = DMA_VECTOR
__interrupt void DMA(){

	if (dma_counter < 3) {
		dma_counter++;
	} else {
		dma_counter = 0;
	}

	DMA0CTL	&=	~DMAIFG; 									// resets the IF of the DMA
	DMA0DA	=	adc12_com.adc_buffer[dma_counter];			// sets the destination address
	adc12_com.Status.B.adc_rdy	=	1; 						// writes a "ready" into the status register for the ADC
}

