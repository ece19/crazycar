/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_GPIO_H_
#define HAL_HAL_GPIO_H_

/*
 * HIGH and LOW for setting/resetting the port registers
 */
#define HIGH 0xFF
#define LOW 0x00

/*
 * config port 1
 */
#define RPM_SENSOR BIT3
#define RPM_SENSOR_DIR BIT4
#define I2C_INT_MOTION BIT5
#define START_BUTTON BIT6
#define STOP_BUTTON BIT7

/*
 * config of port 2
 */
#define DEBUG_TXD BIT0
#define DEBUG_RXD BIT1
#define AUX_PIN_ONE BIT2
#define AUX_PIN_TWO BIT3
#define AUX_PIN_THREE BIT4
#define AUX_PIN_FOUR BIT5
#define I2C_SDA_MOTION BIT6
#define I2C_SCL_MOTION BIT7

/*
 * config of port 3
 */
#define THROTTLE BIT2
#define STEERING BIT3
#define SMCLK BIT4
#define DISTANCE_FRONT_EN BIT7

/*
 * config of port 4
 */
#define LINE_FOLLOW_2 BIT3
#define LINE_FOLLOW_1 BIT4
#define LINE_FOLLOW_3 BIT5
#define LINE_FOLLOW_4 BIT6
#define LINE_FOLLOW_5 BIT7

/*
 * config of port 6
 */
#define DISTANCE_RIGHT BIT0
#define DISTANCE_LEFT BIT1
#define DISTANCE_FRONT BIT2
#define VBAT_MEASURE BIT3
#define DISTANCE_LEFT_EN BIT4

/*
 * config of port 7
 */
#define XT2IN BIT2
#define XT2OUT BIT3

/*
 * config of port 8
 */
#define LCD_BL BIT0
#define LCD_SPI_CS BIT1
#define UART_TXD_AUX BIT2
#define UART_RXD_AUX BIT3
#define LCD_SPI_CLK BIT4
#define LCD_SPI_MOSI BIT5
#define LCD_SPI_MISO BIT6
#define LCD_DATACMD BIT7

/*
 * config of port 9
 */
#define LCD_RESET BIT0
#define DISTANCE_RIGHT_EN BIT7

/*
 * struct for enabling global access to the button state interrupt
 */
typedef struct {
	unsigned char active;
	unsigned char button;
} ButtonCom;

/*
 * struct for enabling global access to the rpm sensor state
 */
typedef struct {
    unsigned char rotation_cnt;
    unsigned char dir;
    unsigned short speed[2];
} RPMCom;

void halGpioInit(void); 		// enabling global access for INIT GPIO

#endif /* HAL_HAL_GPIO_H_ */
