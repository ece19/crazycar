/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_TIMERB0_H_
#define HAL_HAL_TIMERB0_H_

	#define TIMER_COUNT SMCLK_FREQU		// value which is goint to be compared in the cc register

	void halTimerB0Init(void); 			// enabling gloabl access

#endif /* HAL_HAL_TIMERB0_H_ */
