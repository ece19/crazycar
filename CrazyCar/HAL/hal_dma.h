/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_DMA_H_
#define HAL_HAL_DMA_H_

	void halDmaInit(void);

#endif /* HAL_HAL_DMA_H_ */
