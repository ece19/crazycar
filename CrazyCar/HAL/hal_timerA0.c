/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "hal_timerA0.h"
#include "AL/algorithm_general.h"
// --------------------------------------------------

/*
 * configures and sets the timer A0
 * timer clk: SMCLK
 * compare mode: up
 */
void halTimerA0Init(){
    TA0CTL |= TBCLR;            // Timer_B clear

    TA0CTL |= MC_1 | TBSSEL_2;  // up mode, SMCLK

    TA0CTL |=  ID__8;           // Divider 8 for SMCLK

    TA0EX0 |=  TBIDEX__8;       // Divider 8 for SMLCK

    TA0CCTL0 |= CCIE;           // compare mode, enable interrupt

    TA0CCR0 = 648;				// 648 = 60Hz, 488 = 80Hz
}


/*
 * IRS for timber AO
 */
#pragma vector = TIMER0_A0_VECTOR;
__interrupt void TimerA0Interrupt(){
    PIDInit();
}










