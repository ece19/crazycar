/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "hal_ucs.h"
#include "hal_gpio.h"
// --------------------------------------------------

/*
 * configuration for the MCLK, SMCLK and ACLK
 * MCLK: 20MHz
 * SMCLK: 2,5 MHz
 * ACLK: reference clock
 */
void halUcsInit(){
    UCSCTL6 &= ~XT2OFF;										 	// turn on XT2 clock
    UCSCTL3|=SELREF_2; 											// set FLL to REFOCLK
    UCSCTL4|=SELA2; 											// set ACLK to REFOCLK

    while(SFRIFG1 & OFIFG){										// wait until all error flags are cleared
        UCSCTL7 &= ~(XT2OFFG + DCOFFG + XT1HFOFFG + XT1LFOFFG);
        SFRIFG1 &= ~OFIFG;
    }

    UCSCTL6|=XT2DRIVE_2; 										// set the drive strength to 20MHz
    UCSCTL4|=SELM__XT2CLK | SELS__XT2CLK ; 						// Set the master and submaster clock
    UCSCTL5|=DIVS__8; 											// set frequency divider to 8, for getting suitable SMCLK
}
