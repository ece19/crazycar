/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------
#include "DL/driver_ir.h"
#include "HAL/hal_adc12.h"
#include "HAL/hal_gpio.h"
// --------------------------------------------------

void configureCTL0(void);
void configureCTL1(void);
void configureCTL2(void);
void configureMCTL0(void);
void configureMCTL1(void);
void configureMCTL2(void);
void configureMCTL3(void);


ADC12Com adc12_com;

/*
 * configuration of the ADC
 */

void halAdc12Init(){

	ADC12CTL0 &= ~ADC12ENC;
	REFCTL0 |= REFON | REFOUT | REFVSEL1;

	// configuration of CTL0
	configureCTL0();

	// configuration of CTL1
	configureCTL1();

	// configure of CTL2
	configureCTL2();

	// configuration of Memory CTL1
	configureMCTL0();

	// configuration of Memory CTL2
	configureMCTL1();

	// configuration of Memory CTL3
	configureMCTL2();

	// configuration of Memory CTL4
	configureMCTL3();

	//ADC12IE |= ADC12IE3;
	ADC12CTL0 |= ADC12ON;
	ADC12CTL0 |= ADC12ENC;

}

void configureCTL0(){
	ADC12CTL0 |= ADC12SHT0_15;			// defines the duration of the sampling period
	ADC12CTL0 |= ADC12SHT1_15;			// defines the sample and hold time
	ADC12CTL0 |= ADC12REFON;			// activates the reference voltage
	ADC12CTL0 |= ADC12REF2_5V;			// sets reference voltage to 2.5V
	ADC12CTL0 |= ADC12MSC;				// triggers on the rising edge of a signal, repeats furthermore
}

void configureCTL1(){
	ADC12CTL1 |= ADC12SHS_3;			// sample and hold source select -> (possible entries from .h file 1 - 3)
	ADC12CTL1 |= ADC12SSEL_3;			// input source select for the ADC -> SMCLK
	ADC12CTL1 |= ADC12SHP;				// sample and hold pulse mode selection -> SAMPCON source = sampling timer
	ADC12CTL1 |= ADC12CONSEQ_3;			// conversion sequence code selection -> repeat - sequence of channels (1 = sequence of channels)
}

void configureCTL2(){
	ADC12CTL2 |= ADC12RES_2;			// sets the resolution of the ADC
}

void configureMCTL0(){
	ADC12MCTL0 |= ADC12INCH_0;			// sets the analog pin 0 to the ADC MUX INPUT 0
	ADC12MCTL0 |= ADC12SREF_1;			// sets the reference voltage from VREF+ to GND
	//ADC12MCTL0 &= ~ADC12EOS;			// end of sequence -> false
}

void configureMCTL1(){
	ADC12MCTL1 |= ADC12INCH_1;			// sets the analog pin 1 to the ADC MUX INPUT 1
	ADC12MCTL1 |= ADC12SREF_1;			// sets the reference voltage from VREF+ to GND
	//ADC12MCTL1 &= ~ADC12EOS;			// end of sequence -> false
}

void configureMCTL2(){
	ADC12MCTL2 |= ADC12INCH_2;			// sets the analog pin 2 to the ADC MUX INPUT 2
	ADC12MCTL2 |= ADC12SREF_1;			// sets the reference voltage from VREF+ to GND
	//ADC12MCTL2 &= ~ADC12EOS;			// end of sequence -> false
}

void configureMCTL3(){
	ADC12MCTL3 |= ADC12INCH_3;			// sets the analog pin 3 to the ADC MUX INPUT 3
	ADC12MCTL3 |= ADC12SREF_1;			// sets the reference voltage from VREF+ to GND
	ADC12MCTL3 |= ADC12EOS;				// end of sequence -> true
}

/*
 * calculates the median value out of four Values from the same sensor out of the ADC
 */
unsigned short calcMedianFromADCValues(ADC_POS pos) {
    unsigned short sum = 0;
    char i;
    for (i = 0; i < 4; i++) {
        sum += adc12_com.adc_buffer[i][pos];
    }
    return sum >> 2;
}
