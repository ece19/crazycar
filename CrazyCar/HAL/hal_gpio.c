/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "hal_gpio.h"
#include "AL/algorithm_steering.h"
#include "AL/algorithm_general.h"
#include "AL/algorithm_throttle.h"
// --------------------------------------------------

// prototypes for all local functions ---------------
void resetGpio(void);
void configureGpio(void);
void configureButtonsIO(void);
void confgureRPMIO();
void configureClockIO(void);
void configureDriveControlIO(void);
void configureSPIIO(void);
void configureADCIO(void);

__interrupt void P1Interrupts(void);
// --------------------------------------------------

// global variables ----------------------------------
ButtonCom button_com;
RPMCom rpm_com;
extern PIDParams steering_params;
// --------------------------------------------------

/*
 * collects all functions which are used for the GPIO configuration
 */
void halGpioInit(){
    resetGpio();
    configureGpio();
    configureButtonsIO();
    confgureRPMIO();
    configureClockIO();
    configureDriveControlIO();
    configureSPIIO();
    configureADCIO();

    __enable_interrupt();
}

/**
 * configures all GPIO-Pins on every PORT as unused, sets OUTPUT to low
 */
void resetGpio(){
    P1DIR = HIGH;
    P1DS = LOW;
    P1OUT = LOW;

    P2DIR = HIGH;
    P2DS = LOW;
    P2OUT = LOW;

    P3DIR = HIGH;
    P3DS = LOW;
    P3OUT = LOW;

    P4DIR = HIGH;
    P4DS = LOW;
    P4OUT = LOW;

    P5DIR = HIGH;
    P5DS = LOW;
    P5OUT = LOW;

    P6DIR = HIGH;
    P6DS = LOW;
    P6OUT = LOW;

    P7DIR = HIGH;
    P7DS = LOW;
    P7OUT = LOW;

    P8DIR = HIGH;
    P8DS = LOW;
    P8OUT = LOW;

    P9DIR = HIGH;
    P9DS = LOW;
    P9OUT = LOW;
}

/**
 * configures GPIO-Pins so they complement the CrayCar schematics
 * */
void configureGpio(){
	P1DIR = (P1DIR & (~RPM_SENSOR & ~RPM_SENSOR_DIR)) | (I2C_INT_MOTION & ~START_BUTTON & ~STOP_BUTTON);

	P2DIR = (P2DIR & (~DEBUG_TXD))| DEBUG_RXD | AUX_PIN_ONE | AUX_PIN_TWO | AUX_PIN_THREE | (AUX_PIN_FOUR & ~(I2C_SDA_MOTION)) | I2C_SCL_MOTION;

	P3DIR = P3DIR | THROTTLE | STEERING | SMCLK | DISTANCE_FRONT_EN;

	P4DIR = P4DIR & ~LINE_FOLLOW_1  & ~LINE_FOLLOW_2  & ~LINE_FOLLOW_3  & ~LINE_FOLLOW_4  & ~LINE_FOLLOW_5;

	P6DIR = (P6DIR & (~DISTANCE_RIGHT) & (~DISTANCE_LEFT) & (~DISTANCE_FRONT) & (~VBAT_MEASURE))  | DISTANCE_LEFT_EN;

	P7DIR = (P7DIR & (~XT2IN)) | XT2OUT;

	P8DIR = P8DIR | LCD_BL | LCD_SPI_CS | (UART_TXD_AUX& (~UART_RXD_AUX)) | LCD_SPI_CLK | (LCD_SPI_MOSI& (~LCD_SPI_MISO)) | LCD_DATACMD;

	P9DIR = P9DIR | LCD_RESET | DISTANCE_RIGHT_EN;
}

/**
 * set configuration for Start and Stop Button
 * */
void configureButtonsIO() {
	P1REN = P1REN | START_BUTTON | STOP_BUTTON;

	P1OUT = P1OUT | START_BUTTON | STOP_BUTTON;

	P1IE = P1IE | START_BUTTON | STOP_BUTTON;

	P1IES = P1IES & ~START_BUTTON & ~START_BUTTON;
}

void confgureRPMIO() {
    P1REN |= RPM_SENSOR | RPM_SENSOR_DIR;
    P1OUT |= RPM_SENSOR | RPM_SENSOR_DIR;

    P1IE |= RPM_SENSOR | RPM_SENSOR_DIR;
    P1IES &= ~RPM_SENSOR & ~RPM_SENSOR_DIR;
}

/**
 * configures the XT2 clock Pins
 */
void configureClockIO(){
    P7SEL |= XT2IN | XT2OUT ;

    UCSCTL6 &= ~XT2BYPASS;

    P3DIR |= SMCLK; 										//writing the SMCLK to PORT 3, PIN 4, for test purposes

    P3SEL |= SMCLK;
}


/**
 * configure throttle and steering for PWM signals
 */
void configureDriveControlIO() {
    P3SEL |= THROTTLE | STEERING;
}

/**
 * configure pins for SPI
 */
void configureSPIIO() {
    P8SEL |= LCD_SPI_CLK | LCD_SPI_MOSI | LCD_SPI_MISO;
}

/*

 * set and configure the ADC Pins
 */

void configureADCIO(){
	P6SEL &= ~DISTANCE_RIGHT & ~DISTANCE_LEFT & ~DISTANCE_FRONT & ~VBAT_MEASURE;
}

/**
 * function executed on Port 1 Interrupts
 */
#pragma vector = PORT1_VECTOR
__interrupt void P1Interrupts() {
	switch (P1IFG) {
		case START_BUTTON:
			button_com.active = 1;
			button_com.button = 1;
			 steering_params.kd++;
            P1IFG &= ~START_BUTTON;
			break;
		case STOP_BUTTON:
			button_com.active = 1;
			button_com.button = 0;
			steering_params.kd--;
			P1IFG &= ~STOP_BUTTON;
			break;
		case RPM_SENSOR | RPM_SENSOR_DIR:
		    if (P1OUT | RPM_SENSOR_DIR) rpm_com.dir = 1;
		    else rpm_com.dir = 0;
		    P1IFG &= ~RPM_SENSOR_DIR;
		    /* no break */
		case RPM_SENSOR:
		    rpm_com.rotation_cnt++;
		    P1IFG &= ~RPM_SENSOR;
		    break;
		default:
		    P1IFG = LOW;
		    break;
	}

}










