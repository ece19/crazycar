/*
 * hal_usciB1.h
 *
 *  Created on: 18.11.2020
 *      Author: lukas
 */

#ifndef HAL_HAL_USCIB1_H_
#define HAL_HAL_USCIB1_H_

void halUSCIB1Init(void);
void halUSCIB1Transmit(void);

typedef struct {
    union{
        unsigned char R;
        struct {
            unsigned char tx_success: 1;    // =1 when data is send
            unsigned char dummy: 7;
        }B;
    }Status;

    struct {
        unsigned char len;                  // length of transmitted data in bytes
        unsigned char cnt;                  // index of the transmitting byte
        unsigned char data[256];            // TX data
    }TxData;

    struct {
        unsigned char len;                  // length of transmitted data in bytes
        unsigned char data[256];            // RX data
    }RxData;
}USCIB1SPICom;

#endif /* HAL_HAL_USCIB1_H_ */
