/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "hal_wdt.h"
// --------------------------------------------------

/*
 * INIT the watchdog timer
 * default: stop mode
 */
void halWdtInit() {
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
}
