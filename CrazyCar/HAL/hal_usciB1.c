/*
 * hal_usciB1.c
 *
 *  Created on: 18.11.2020
 *      Author: lukas
 */

#include <msp430.h>
#include "hal_usciB1.h"
#include "hal_gpio.h"
#include "hal_adc12.h"

__interrupt void USCI1(void);

USCIB1SPICom lcd_data;
//extern ADC12Com adc12_com;

void halUSCIB1Init() {
    P8OUT |= LCD_SPI_CS;
    UCB1CTL1 |= UCSWRST;    // Reset

    UCB1CTL1 |= UCSSEL_2;    // Select SMCLK
    UCB1BRW = 25;

    UCB1CTL0 |= UCMST;
    UCB1CTL0 &= ~UC7BIT;
    UCB1CTL0 &= ~UCCKPH;
    UCB1CTL0 |= UCCKPL;
    UCB1CTL0 |= UCMSB;

    UCB1CTL1 &= ~UCSWRST;

    UCB1IE |= UCRXIE;

    lcd_data.Status.B.tx_success = 1;
}

void halUSCIB1Transmit() {
    lcd_data.TxData.cnt = 0;
    lcd_data.Status.B.tx_success = 0;

    if (lcd_data.TxData.cnt < lcd_data.TxData.len) {
        P8OUT &= ~LCD_SPI_CS;
        UCB1TXBUF = lcd_data.TxData.data[lcd_data.TxData.cnt];
    }
}


#pragma vector = USCI_B1_VECTOR;
__interrupt void USCI1() {
    if (UCB1IFG & UCRXIFG) {
        if (!lcd_data.Status.B.tx_success) {
            P8OUT |= LCD_SPI_CS;

            lcd_data.RxData.data[lcd_data.TxData.len] = UCB1RXBUF;
            lcd_data.RxData.len++;

            lcd_data.TxData.cnt++;
            if (lcd_data.TxData.cnt < lcd_data.TxData.len) {
                P8OUT &= ~LCD_SPI_CS;
                UCB1TXBUF = lcd_data.TxData.data[lcd_data.TxData.cnt];
            } else {
                lcd_data.Status.B.tx_success = 1;
            }
        }
    }

}
