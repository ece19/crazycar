/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_WDT_H_
#define HAL_HAL_WDT_H_

 void halWdtInit(void); // enabling global access for the watchdog timer

#endif /* HAL_HAL_WDT_H_ */
