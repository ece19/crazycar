/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_TIMERA0_H_
#define HAL_HAL_TIMERA0_H_

void halTimerA0Init(void);

#endif /* HAL_HAL_TIMERA0_H_ */
