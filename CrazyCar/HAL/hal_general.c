/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

/*
 * Imports of all INIT functions for every peripheral
 */
// --------------------------------------------------
#include "hal_pmm.h"
#include "hal_gpio.h"
#include "hal_wdt.h"
#include "hal_general.h"
#include "hal_ucs.h"
#include "hal_timerB0.h"
#include "hal_timerA0.h"
#include "hal_timerA1.h"
#include "hal_usciB1.h"
#include "hal_adc12.h"
#include "hal_dma.h"
// --------------------------------------------------

/*
 * collects all INIT functions for every peripheral
 */
void halInit(){
    halWdtInit();				// watchdog timer

    HAL_PMM_Init();				// power management

    halGpioInit();				// GPIO pins

    halUcsInit();				// unified clock system

    halTimerB0Init();			// timer B0

    halTimerA0Init();           // timer A0

    halTimerA1Init();			// timer A1

    halUSCIB1Init();            // SPI

    halAdc12Init(); 			// ADC

    halDmaInit(); 				// DMA

}

