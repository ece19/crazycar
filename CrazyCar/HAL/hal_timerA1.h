/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_TIMERA1_H_
#define HAL_HAL_TIMERA1_H_

#define SERVO_M 3550
#define SERVO_L 2800
#define SERVO_R 4200
#define SERVO_STEP 7

#define THROTTLE_FORWARD_MAX 10000
#define THROTTLE_FORWARD_MIN 7500
#define THROTTLE_REVERSE_MAX 2500
#define THROTTLE_REVERSE_MIN 5000
#define THROTTLE_BREAK 6250
#define THROTTLE_STEP 25

void halTimerA1Init(void);

#endif /* HAL_HAL_TIMERA1_H_ */
