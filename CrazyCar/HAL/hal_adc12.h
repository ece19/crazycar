/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_ADC12_H_
#define HAL_HAL_ADC12_H_

	typedef enum { IR_RIGHT, IR_LEFT, IR_FRONT, IR_BATTERY} ADC_POS; 		// referes to the correct order of reading the sensors;

	typedef struct {
		union {
			unsigned char R;
			struct {
				unsigned char adc_rdy : 1;
				unsigned char dummy : 7;
			}B;
		}Status;
		unsigned short adc_buffer[4][4];
	} ADC12Com;

	void halAdc12Init(void);
	unsigned short calcMedianFromADCValues(ADC_POS pos);

#endif /* HAL_HAL_ADC12_H_ */
