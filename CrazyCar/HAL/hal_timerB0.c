/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "hal_timerB0.h"
#include "hal_ucs.h"
#include "hal_gpio.h"
#include "DL/driver_lcd.h"
// --------------------------------------------------

// --------------------------------------------------
#define TIMER_FREQ 30

extern RPMCom rpm_com;
char rpm_freq = 0;
char rpm_cnt = 0;
// --------------------------------------------------

/*
 * configures and sets the timer B0
 * timer clk: SMCLK
 * compare mode: up
 */
void halTimerB0Init(){
    TB0CTL |= TBCLR;            // Timer_B clear

    TB0CTL |= MC_1 | TBSSEL_2;  // up mode, SMCLK

    TB0CTL |=  ID__8;           // Divider 8 for SMCLK

    TB0EX0 |=  TBIDEX__8;       // Divider 8 for SMLCK

    TB0CCTL0 |= CCIE;           // compare mode, enable interrupt

    TB0CCR0 = 324;

    TB0CCTL1 |= OUTMOD_7;

    TB0CCR1 = 1;
}


/*
 * IRS for timber BO
 */
#pragma vector = TIMERB0_VECTOR;
__interrupt void TimerB0Interrupt(){
    rpm_freq++;

    if (rpm_freq >= 4) {
        rpm_com.speed[rpm_cnt] = rpm_com.rotation_cnt * TIMER_FREQ * 10;
        rpm_com.rotation_cnt = 0;
        rpm_freq = 0;

        if (rpm_cnt < 2) {
            rpm_cnt ++;
        } else {
            rpm_cnt = 0;
        }
    }
}










