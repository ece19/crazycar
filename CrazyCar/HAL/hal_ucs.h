/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_UCS_H_
#define HAL_HAL_UCS_H_

	#define XTAL_FREQU 20000000		// frequency of XT2 out
	#define MCLK_FREQU 20000000		// frequency of MCLK
	#define SMCLK_FREQU 2500000		// frequency of SMCLK

    void halUcsInit(void);

#endif /* HAL_HAL_UCS_H_ */
