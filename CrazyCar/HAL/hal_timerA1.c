/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "hal_timerA1.h"
#include "hal_ucs.h"
#include "hal_gpio.h"
// --------------------------------------------------

// local prototype functions ------------------------
void configureThrottle(void);
void configureSteering(void);
// --------------------------------------------------

/**
 * Initialize TimerA1 for PWM
 */
void halTimerA1Init() {
    TA1CTL |= TBCLR; 			// Timer_A clear
    TA1CTL |= MC_1 | TASSEL_2;

    TA1CCTL0 &= ~CAP;
    TA1CCTL0 |= CCIE;           // enable Interrupt
    TA1CCR0 = SMCLK_FREQU / 60; // Set pulse duration

    configureThrottle();
    configureSteering();
}

/**
 * Configure PWM output for Throttle
 */
void configureThrottle() {
    TA1CCTL1 &= ~CAP;
    TA1CCTL1 |= OUTMOD_7;   	// Outmode Reset/set
    TA1CCR1 = 0;           		// Set pulse duration
}

/**
 * Configure PWM output for Steering
 */
void configureSteering() {
    TA1CCTL2 &= ~CAP;
    TA1CCTL2 |= OUTMOD_7;   	// Outmode Reset/set
    TA1CCR2 = 0;            	// Set pulse duration
}

