/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef HAL_HAL_GENERAL_H_
#define HAL_HAL_GENERAL_H_

    void halInit(); 			// enables accessing the function in other files

#endif /* HAL_HAL_GENERAL_H_ */
