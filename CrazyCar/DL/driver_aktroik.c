/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include "msp430.h"
// --------------------------------------------------

// --------------------------------------------------
#include "driver_aktorik.h"
#include "HAL/hal_timerA1.h"
// --------------------------------------------------

// local prototype functions ------------------------
void driverThrottleCalibrate(short speed);
// --------------------------------------------------

// global variables ---------------------------------
short pulse_counter = 0;
enum {RUNNING, END} calibration = RUNNING;
// --------------------------------------------------

/**
 * Initialize steering
 */
void driverSteeringInit(){
    TA1CCR2 = SERVO_M;
}

/**
 * Initial throttle calibration
 */
void driverThrottleInit(){
    driverThrottleCalibrate(THROTTLE_REVERSE_MAX);
    driverThrottleCalibrate(THROTTLE_REVERSE_MIN);
    driverThrottleCalibrate(THROTTLE_FORWARD_MIN);
    driverThrottleCalibrate(THROTTLE_FORWARD_MAX);
    driverThrottleCalibrate(THROTTLE_BREAK);

    calibration = END;
}

/**
 * Set given speed for 140 pulses (throttle calibration)
 */
void driverThrottleCalibrate(short speed) {
    while (pulse_counter < 140)

    TA1CCR1 = speed;
    pulse_counter = 0;
}

/**
 * Set steering angle by given value from -100 (max left) to 100 (max right)
 */
void driverSetSteering(signed short percent){
    if (percent > STEERING_RANGE_R) {
        TA1CCR2 = SERVO_R;
    } else if (percent < STEERING_RANGE_L) {
        TA1CCR2 = SERVO_L;
    } else {
        TA1CCR2 = SERVO_M + percent * SERVO_STEP;
    }
}

/**
 * Set throttle speed by given value from -100 (max reverse) to 100 (max forward)
 */
void driverSetThrottle(signed short percent){
    if (percent > THROTTLE_RANGE_F) {
        TA1CCR1 = THROTTLE_FORWARD_MAX;
    } else if (percent > 0) {
        TA1CCR1 = THROTTLE_FORWARD_MIN + percent * THROTTLE_STEP;
    } else if (percent < THROTTLE_RANGE_R) {
        TA1CCR1 = THROTTLE_REVERSE_MAX;
    } else if (percent < 0) {
        TA1CCR1 = THROTTLE_REVERSE_MIN + percent * THROTTLE_STEP;
    } else {
        TA1CCR1 = THROTTLE_BREAK;
    }
}

#pragma vector = TIMER1_A0_VECTOR
__interrupt void escCalibration() {
        if (calibration == RUNNING) {
            pulse_counter++;
        }
}




