/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef DL_DRIVER_LCD_H_
#define DL_DRIVER_LCD_H_

	#define DISPLAY_RESET 0xE2
	#define LCD_BIAS 0xA3
	#define ADC_SEL_NORMAL 0xA0
	#define COMMON_REVERSE 0xC8
	#define RES_RATIO 0x24
	#define ELEC_VOL_MODE 0x81
	#define ELEC_VOL_VALUE 0x0F
	#define POWER_CONT 0x2F
	#define DISPLAY_ON 0xAF

	#define LCD_COMMAND (P8OUT &= ~LCD_DATACMD)
	#define LCD_DATA (P8OUT |= LCD_DATACMD)

	#define LCD_BACKLIGHT_ON (P8OUT |= LCD_BL)

	#define FIRST_PAGE 0
	#define LAST_PAGE 8
	#define MAX_COLLUMN 128
	#define MAX_CHARACTER	(MAX_COLLUMN / 6)


	void driverLCDInit(void);
	void driverLCDWriteCommand(unsigned char* data, unsigned char data_length);
	void driverLCDSetPosition(unsigned char page, unsigned char col);
	void driverLCDClear(void);
	void driverLCDWriteUint(unsigned short number, unsigned char page, unsigned char col);
	void driverLCDWriteText(char *text, unsigned char text_length, unsigned char page, unsigned char col);


#endif /* DL_DRIVER_LCD_H_ */
