/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#include "driver_rpm.h"
#include "HAL/hal_gpio.h"

extern RPMCom rpm_com;

void driverRpmInit() {
  rpm_com.dir = 1;
  rpm_com.rotation_cnt = 0;
}

/*
 * 	returns speed in positive values 0 - 4000
 */
unsigned short RPMSpeed() {
    unsigned short sum = 0;
    char i;

    for (i = 0; i < 2; i++) {
        sum += rpm_com.speed[i];
    }

    return sum >> 1;
}
