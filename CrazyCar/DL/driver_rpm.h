/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef DL_DRIVER_RPM_H_
#define DL_DRIVER_RPM_H_

void driverRpmInit(void);
unsigned short RPMSpeed(void);

#endif /* DL_DRIVER_RPM_H_ */
