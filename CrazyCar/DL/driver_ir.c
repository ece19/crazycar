/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "driver_ir.h"
#include "driver_ir_lut.h"
#include "HAL/hal_adc12.h"
// --------------------------------------------------


unsigned short IRFrontDistance() {
    unsigned short adc_val = calcMedianFromADCValues(IR_FRONT) >> 3;

    if (adc_val >= 57) {            // 57 max. Distanz
        return ir_front[adc_val];
    }

    return 2200; //65535
}

unsigned short IRLeftDistance() {
    unsigned short adc_val = calcMedianFromADCValues(IR_LEFT) >> 3;

    if (adc_val >= 68) {            // 68 max. Distanz
        return ir_left[adc_val];
    }

    return 1200; //65535;
}

unsigned short IRRightDistance() {
    unsigned short adc_val = calcMedianFromADCValues(IR_RIGHT) >> 3;

    if (adc_val >= 51) {            // 51 max. Distanz
        return ir_right[adc_val];
    }

    return 1200; //65535;
}
