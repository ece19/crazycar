/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

/*
 * Imports of all INIT functions for every driver layer module
 */
// --------------------------------------------------
#include "driver_general.h"
#include "driver_aktorik.h"
#include "driver_rpm.h"
#include "driver_lcd.h"
// --------------------------------------------------

/**
 * Init driver layer
 */
void driverInit(){
    driverSteeringInit();
	driverThrottleInit();
	driverRpmInit();
	driverLCDInit();
}
