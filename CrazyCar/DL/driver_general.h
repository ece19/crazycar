/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef DL_DRIVER_GENERAL_H_
#define DL_DRIVER_GENERAL_H_

	void driverInit(void);

#endif /* DL_DRIVER_GENERAL_H_ */
