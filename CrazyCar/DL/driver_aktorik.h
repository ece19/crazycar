/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */
#ifndef DL_DRIVER_AKTORIK_H_
#define DL_DRIVER_AKTORIK_H_

#define STEERING_RANGE_L -100
#define STEERING_RANGE_R 100

#define THROTTLE_RANGE_F 100
#define THROTTLE_RANGE_R -100

	void driverSteeringInit(void);
	void driverThrottleInit(void);
	void driverSetSteering(signed short percent);
	void driverSetThrottle(signed short percent);

#endif /* DL_DRIVER_AKTORIK_H_ */
