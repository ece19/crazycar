/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

#ifndef DL_DRIVER_IR_H_
#define DL_DRIVER_IR_H_

unsigned short IRFrontDistance();
unsigned short IRLeftDistance();
unsigned short IRRightDistance();

#endif /* DL_DRIVER_IR_H_ */
