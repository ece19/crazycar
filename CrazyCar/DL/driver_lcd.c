/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include <msp430.h>
// --------------------------------------------------

// --------------------------------------------------
#include "driver_lcd.h"
#include "HAL/hal_gpio.h"
#include "HAL/hal_usciB1.h"
#include "driver_lcd_font_table.h"
// --------------------------------------------------

extern USCIB1SPICom lcd_data;
unsigned char lcd_pos[3];

void driverLCDInit(){
	LCD_BACKLIGHT_ON;

	P9OUT &= ~LCD_RESET;
	__delay_cycles(500000);
	P9OUT |= LCD_RESET;

	unsigned char lcd_init_array[9] = {
			DISPLAY_RESET,
			LCD_BIAS,
			ADC_SEL_NORMAL,
			COMMON_REVERSE,
			RES_RATIO,
			ELEC_VOL_MODE,
			ELEC_VOL_VALUE,
			POWER_CONT,
			DISPLAY_ON
	};

	driverLCDWriteCommand(lcd_init_array, 9);

	driverLCDClear();
}

void driverLCDWriteCommand(unsigned char* data, unsigned char data_length){
	unsigned char i;

	while(lcd_data.Status.B.tx_success == 0);

	LCD_COMMAND;

	for(i = 0; i < data_length; i++){
		lcd_data.TxData.data[i] = *data;
		data++;
	}

	lcd_data.TxData.len = data_length;
	lcd_data.TxData.cnt = 0;

	halUSCIB1Transmit();
	while(lcd_data.Status.B.tx_success == 0);
}

void driverLCDSetPosition(unsigned char page, unsigned char col){
	unsigned char msb_col = (col >> 4) | (0x10);
	unsigned char lsb_col = (col & 0x0F);
	unsigned char b_page = page|0xB0;

	lcd_pos[0] = b_page;
	lcd_pos[1] = msb_col;
	lcd_pos[2] = lsb_col;

	driverLCDWriteCommand(lcd_pos, 3);
}

void driverLCDClear(){
	unsigned char i, j;
	for(i = 0; i < LAST_PAGE; i++){
		driverLCDSetPosition(i, 0);
		LCD_DATA;

		for(j = 0; j < MAX_COLLUMN; j++){
			lcd_data.TxData.data[j] = 0;
		}

		lcd_data.TxData.len = MAX_COLLUMN;

		halUSCIB1Transmit();
		while(lcd_data.Status.B.tx_success == 0);
	}
}

void driverLCDWriteUint(unsigned short number, unsigned char page, unsigned char col){

	char text[5] = {0}, i = 1;

	do {
	    text[5 - i] = number % 10 + '0';
	    number /= 10;
	    i++;
	} while (number > 0);

	driverLCDWriteText(text, 5, page, col);

}

void driverLCDWriteText(char *text, unsigned char text_length, unsigned char page, unsigned char col){

	unsigned char i, text_length_cnt, col_pos = col, page_pos = page;

	driverLCDSetPosition(page, col);
	LCD_DATA;

	for(text_length_cnt = text_length; text_length_cnt > 0; text_length_cnt--){

		if(col_pos + FONTS_WIDTH_MAX >= MAX_COLLUMN){
			col_pos = 0;
			page_pos++;
			driverLCDSetPosition((page_pos), col_pos);
			LCD_DATA;
		}

		if(page_pos >= LAST_PAGE)break;


		for(i = 0; i < FONTS_WIDTH_MAX; i++){
			lcd_data.TxData.data[i] = font_table[*text][i];
			col_pos++;
		}

		lcd_data.TxData.len = FONTS_WIDTH_MAX;

		halUSCIB1Transmit();
		while(lcd_data.Status.B.tx_success == 0);

		text++;
	}
}















