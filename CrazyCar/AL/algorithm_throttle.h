/*
 * algorithm-throttle.h
 *
 *  Created on: 19.01.2021
 *      Author: tim
 */

#ifndef AL_ALGORITHM_THROTTLE_H_
#define AL_ALGORITHM_THROTTLE_H_

	#define HIGH_SPEED 3500
	#define LOW_SPEED 1600
	#define SUPER_LOW_SPEED 400

	void throttleController(void);

#endif /* AL_ALGORITHM_THROTTLE_H_ */
