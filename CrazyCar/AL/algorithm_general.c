/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include "msp430.h"
// --------------------------------------------------

// --------------------------------------------------
#include "algorithm_steering.h"
#include "algorithm_throttle.h"
// --------------------------------------------------

void PIDInit() {
    steeringController();
	throttleController();
}
