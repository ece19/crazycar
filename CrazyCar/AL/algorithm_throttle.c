/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include "msp430.h"
// --------------------------------------------------

// --------------------------------------------------
#include "algorithm_general.h"
#include "algorithm_throttle.h"
#include "DL/driver_aktorik.h"
#include "DL/driver_rpm.h"
#include "DL/driver_ir.h"
#include "HAL/hal_timerA1.h"
// --------------------------------------------------

PIDParams throttle_params = {0, 0, 0, 1, 2, 2, 0, 0, 0};

short front_distance, reference_throttle;

void throttleController(){

	front_distance = IRFrontDistance();	// getting current front-sensor value

	/*
	 * Determination of the reference value by the front-sensor state
	 */
	//-----------------------------------------------------------------------------------------------------------------------
	if(front_distance >= 1500) {
		reference_throttle = HIGH_SPEED;
	} else if (front_distance <= 100) {
		reference_throttle = SUPER_LOW_SPEED;
	} else {
		reference_throttle = LOW_SPEED;
	}
	//-----------------------------------------------------------------------------------------------------------------------
	/*
	 * calculation of the error and intergral sum
	 */
	//-----------------------------------------------------------------------------------------------------------------------
	throttle_params.e = reference_throttle - RPMSpeed();
	throttle_params.eialt = throttle_params.ei + throttle_params.e;
	//-----------------------------------------------------------------------------------------------------------------------
	/*
	 * calculation of the output variable of the PID Controller
	 */
	//-----------------------------------------------------------------------------------------------------------------------
	throttle_params.u = (throttle_params.kp  * throttle_params.e) >> 3;
	throttle_params.u += (throttle_params.kd * (throttle_params.e - throttle_params.ealt) * TAd) >> 7;
	throttle_params.u += (throttle_params.ki * throttle_params.eialt) >> BSHIFT_TAI;
	//-----------------------------------------------------------------------------------------------------------------------

	throttle_params.ealt = throttle_params.e; // setting the current error to the old error for the differential element

	/*
	 * Anti - Wind UP
	 */
	//-----------------------------------------------------------------------------------------------------------------------
	if(throttle_params.u < 100 && throttle_params.u > -100){
		throttle_params.ei = throttle_params.eialt;
	} else if(throttle_params.u > 100){
		throttle_params.u = 100;
	} else if(throttle_params.u < -100){
		throttle_params.u = -100;
	}
	//-----------------------------------------------------------------------------------------------------------------------

	/*
	 * setting the evaluated output parameter into the throttle function
	 */
	//-----------------------------------------------------------------------------------------------------------------------
	driverSetThrottle(throttle_params.u);
	//-----------------------------------------------------------------------------------------------------------------------

}


