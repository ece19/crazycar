/*
 * algorithm_general.h
 *
 *  Created on: 13.01.2021
 *      Author: lukas
 */

#ifndef AL_ALGORITHM_GENERAL_H_
#define AL_ALGORITHM_GENERAL_H_

#define TA 60
#define TAd 6

#define BSHIFT_TAI	9

typedef struct {
    signed short e;
    signed short ealt;
    signed short esum;
    signed short kp;
    signed short ki;
    signed short kd;
    signed short u;
    signed short eialt;
    signed short ei;
} PIDParams;

typedef struct {
    signed short min_threshhold;
    signed short max_threshhold;
	signed short static_distance;
	char hard_turn_left;
	char hard_turn_right;
	char delay_cnt;
} SensorParams;

void PIDInit(void);

#endif /* AL_ALGORITHM_GENERAL_H_ */
