/*
 * algorithm_steering.h
 *
 *  Created on: 13.01.2021
 *      Author: lukas
 */

#ifndef AL_ALGORITHM_STEERING_H_
#define AL_ALGORITHM_STEERING_H_

#define DELAY_TICKS 0

void steeringController(void);

#endif /* AL_ALGORITHM_STEERING_H_ */
