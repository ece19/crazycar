/*
 * project: CrazyCar
 * university: Fh Joanneum Graz
 * department: electronics and computer engineering
 * lecture: embedded system
 * year: ECE19
 * authors: Schmid, Schüttler
 *
 */

// --------------------------------------------------
#include "msp430.h"
// --------------------------------------------------

// --------------------------------------------------
#include "algorithm_general.h"
#include "algorithm_steering.h"
#include "DL/driver_aktorik.h"
#include "DL/driver_ir.h"
#include "HAL/hal_timerA1.h"
// --------------------------------------------------

PIDParams steering_params	= {0, 0, 0, 1, 0, 2, 0, 0, 0} ;
SensorParams sensor_params  = {380, 1000, 310, 0, 0, 0};

void steeringController(){

    if (sensor_params.delay_cnt <= 0) {
        if((IRRightDistance() > sensor_params.max_threshhold || sensor_params.hard_turn_right) && !sensor_params.hard_turn_left){
        	steering_params.kp = 2;
        	//steering_params.kd = 3;
            steering_params.e = IRRightDistance() - sensor_params.static_distance;

            if(IRLeftDistance() < sensor_params.min_threshhold){
                sensor_params.hard_turn_right = 0;
                sensor_params.delay_cnt = DELAY_TICKS;
                steering_params.kp = 1;
                steering_params.kd = 2;
            } else {
                sensor_params.hard_turn_right = 1;
            }

        } else if((IRLeftDistance() > sensor_params.max_threshhold || sensor_params.hard_turn_left) && !sensor_params.hard_turn_right) {
        	steering_params.kp = 2;
        	//steering_params.kd = 3;

            steering_params.e = sensor_params.static_distance - IRLeftDistance();

            if(IRRightDistance() < sensor_params.min_threshhold){
                sensor_params.hard_turn_left = 0;
                sensor_params.delay_cnt = DELAY_TICKS;
                steering_params.kp = 1;
                steering_params.kd = 2;
            } else {
                sensor_params.hard_turn_left = 1;
            }

        } else {
        	steering_params.kp = 1;
            steering_params.e = IRRightDistance() - IRLeftDistance();
        }
    } else {
    	steering_params.kp = 1;
        sensor_params.delay_cnt--;
        steering_params.e = IRRightDistance() - IRLeftDistance();
    }

    // pid part of the steering controller
    steering_params.u = steering_params.kp * steering_params.e >> 3;
    steering_params.u += steering_params.kd * (steering_params.e - steering_params.ealt) * TAd >> 7;

    steering_params.ealt = steering_params.e;

    driverSetSteering(steering_params.u);
}
